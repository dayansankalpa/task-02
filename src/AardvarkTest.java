import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AardvarkTest {

	@Test
	public void test(CollateGuessGrid) {
		Aardvark testing = new Aardvark();
		testing._g = new Aardvark();
		testing._g.add(new Domino(1, 2));
		testing._g.add(new Domino(3, 4));
		testing._g.add(new Domino(5, 6));
		
		testing.CollateGuessGrid();
		
		assertEquals(1, testing.gg[0][0]);
		assertEquals(1, testing.gg[0][1]);
		assertEquals(1, testing.gg[1][0]);
		assertEquals(1, testing.gg[1][1]);
		assertEquals(1, testing.gg[2][0]);
		assertEquals(1, testing.gg[2][1]);
		assertEquals(1, testing.gg[3][0]);
		assertEquals(1, testing.gg[3][1]);
	}

}
